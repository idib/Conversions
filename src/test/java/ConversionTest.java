import Conversions.Conversion;

import java.text.ParseException;
import java.text.SimpleDateFormat;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

class ConversionTest {

	@org.junit.jupiter.api.Test
	void attitudes() {
		Conversion conv = new Conversion();
		assertEquals(conv.attitudes("USD", "USD"), 0);
	}

	@org.junit.jupiter.api.Test
	void attitudesDiff() {
		Conversion conv = new Conversion();
		assertEquals(conv.attitudes("USD", "EUR"), 0);
		assertEquals(conv.attitudes("USD", "UZS"), 0);
	}

	@org.junit.jupiter.api.Test
	void attitudesDate() {
		Conversion conv = new Conversion("2018-02-12");
		assertEquals(conv.attitudes("USD", "EUR"), 1.2266);
		conv = new Conversion("2018-02-11");
		assertEquals(conv.attitudes("USD", "EUR"), 1.2252);
	}

	@org.junit.jupiter.api.Test
	void attitudesDiffFrom(){
		Conversion conv = new Conversion("2018-02-12");
		assertEquals(conv.attitudes("EUR", "EUR"), 1);
		assertEquals(conv.attitudes("EUR", "USD"), 0.8152);
	}

	@org.junit.jupiter.api.Test
	void attitudesRange(){
		Conversion conv = new Conversion("2018-02-10-2018-02-12");
		assertEquals(conv.attitudes("USD", "USD"), 1);
		assertEquals(conv.attitudes("Euro", "RUB"), 0.0140);
		assertEquals(conv.attitudes("RUB", "EUR"), 71.3139);
		assertEquals(conv.attitudes("USD", "USD", "2018-02-12"), 1);
		assertEquals(conv.attitudes("Euro", "RUB", "2018-02-12"), 0.0140);
		assertEquals(conv.attitudes("RUB", "EUR", "2018-02-12"), 71.3139);
		assertEquals(conv.attitudes("USD", "USD", "2018-02-11"), 1);
		assertEquals(conv.attitudes("Euro", "RUB", "2018-02-11"), 0.0139);
		assertEquals(conv.attitudes("RUB", "EUR", "2018-02-11"), 71.6491);
		assertEquals(conv.attitudes("USD", "USD", "2018-02-10"), 1);
		assertEquals(conv.attitudes("Euro", "RUB", "2018-02-10"), 0.0139);
		assertEquals(conv.attitudes("RUB", "EUR", "2018-02-10"), 71.6549);
	}

	@org.junit.jupiter.api.Test
	void attitudesRangeDateFormat() throws ParseException {
		Conversion conv = new Conversion("2018-02-10-2018-02-12");
		assertEquals(conv.attitudes("USD", "USD"), 1);
		assertEquals(conv.attitudes("Euro", "RUB"), 0.0140);
		assertEquals(conv.attitudes("RUB", "EUR"), 71.3139);
		SimpleDateFormat ft = new SimpleDateFormat("yyyy-MM-dd");
		assertEquals(conv.attitudes("USD", "USD", ft.parse("2018-02-12")), 1);
		assertEquals(conv.attitudes("Euro", "RUB", ft.parse("2018-02-12")), 0.0140);
		assertEquals(conv.attitudes("RUB", "EUR", ft.parse("2018-02-12")), 71.3139);
		assertEquals(conv.attitudes("USD", "USD", ft.parse("2018-02-11")), 1);
		assertEquals(conv.attitudes("Euro", "RUB", ft.parse("2018-02-11")), 0.0139);
		assertEquals(conv.attitudes("RUB", "EUR", ft.parse("2018-02-11")), 71.6491);
		assertEquals(conv.attitudes("USD", "USD", ft.parse("2018-02-10")), 1);
		assertEquals(conv.attitudes("Euro", "RUB", ft.parse("2018-02-10")), 0.0139);
		assertEquals(conv.attitudes("RUB", "EUR", ft.parse("2018-02-10")), 71.6549);
	}

	@org.junit.jupiter.api.Test
	void attitudesExtrapolation() throws ParseException {
		Conversion conv = new Conversion("2018-02-10-2018-02-12");
		assertEquals(conv.attitudes("USD", "USD"), 1);
		assertEquals(conv.attitudes("Euro", "RUB"), 0.0140);
		assertEquals(conv.attitudes("RUB", "EUR"), 71.3139);
		SimpleDateFormat ft = new SimpleDateFormat("yyyy-MM-dd");
		assertEquals(conv.attitudesExtrapolation("USD", "USD", ft.parse("2018-02-13")), 1);
		assertNotEquals(conv.attitudesExtrapolation("Euro", "RUB", ft.parse("2018-02-13")), 0.0140);
		assertNotEquals(conv.attitudesExtrapolation("RUB", "EUR", ft.parse("2018-02-13")), 71.3139);
	}

	@org.junit.jupiter.api.Test
	void attitudesExtrapolationDiff() throws ParseException {
		Conversion conv = new Conversion("2018-02-10-2018-02-12");
		assertEquals(conv.attitudes("USD", "USD"), 1);
		assertEquals(conv.attitudes("Euro", "RUB"), 0.0140);
		assertEquals(conv.attitudes("RUB", "EUR"), 71.3139);
		SimpleDateFormat ft = new SimpleDateFormat("yyyy-MM-dd");
		assertEquals(conv.attitudesExtrapolation("USD", "USD", ft.parse("2018-02-13")), 1);
		assertNotEquals(conv.attitudesExtrapolation("Euro", "RUB", ft.parse("2018-02-13")), 1);
		assertNotEquals(conv.attitudesExtrapolation("RUB", "EUR", ft.parse("2018-02-13")), 71.3139);
	}

	@org.junit.jupiter.api.Test
	void attitudesExtrapolationDiffNotRandom() throws ParseException {
		Conversion conv = new Conversion("2018-02-10-2018-02-12");
		assertEquals(conv.attitudes("USD", "USD"), 1);
		assertEquals(conv.attitudes("Euro", "RUB"), 0.0140);
		assertEquals(conv.attitudes("RUB", "EUR"), 71.3139);
		SimpleDateFormat ft = new SimpleDateFormat("yyyy-MM-dd");
		double a = conv.attitudesExtrapolation("USD", "USD", ft.parse("2018-02-13"));
		double b = conv.attitudesExtrapolation("USD", "USD", ft.parse("2018-02-13"));
		assertEquals(a,b);

		a = conv.attitudesExtrapolation("Euro", "RUB", ft.parse("2018-02-13"));
		b = conv.attitudesExtrapolation("Euro", "RUB", ft.parse("2018-02-13"));
		assertEquals(a,b);

		a = conv.attitudesExtrapolation("RUB", "EUR", ft.parse("2018-02-13"));
		b = conv.attitudesExtrapolation("RUB", "EUR", ft.parse("2018-02-13"));
		assertEquals(a,b);
	}
}