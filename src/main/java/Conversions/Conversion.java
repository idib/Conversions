package Conversions;

import cMath.funcs.Polynomial;

import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class Conversion {

	private final String pattern = "^(?<acronym>[A-Z]+)\\W(?<full>.+)\\W(?<to>\\d+\\.\\d+)|(?<date>\\d{4}-\\d{2}-\\d{2})";
	Pattern pat = Pattern.compile(pattern);
	String namefile;
	Map<String, Long> convTable = new HashMap<>();
	String lastDate;
	Map<String, Map<String, Long>> history = new HashMap<>();
	SimpleDateFormat dateFormater = new SimpleDateFormat("yyyy-MM-dd");

	public Conversion() {

	}

	public Conversion(String namefile) {
		this.namefile = namefile;
		File file = new File(getClass().getResource(namefile).getPath());
		try {
			read(file);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void read(File file) throws Exception {
		Scanner in = new Scanner(file);
		while (in.hasNextLine()) {
			Matcher mat = pat.matcher(in.nextLine());
			mat.find();
			if (mat.group("date") != null) {
				convTable = new HashMap<>();
				String date = mat.group("date");
				lastDate = date;
				history.put(date, convTable);
			} else if (mat.group("acronym") != null && mat.group("full") != null && mat.group("to") != null) {
				String acronym = mat.group("acronym");
				String full = mat.group("full");
				String to = mat.group("to");
				double val = Double.parseDouble(to);
				convTable.put(acronym, (long) (val * 10000));
				convTable.put(full, (long) (val * 10000));
			} else throw new Exception("input File not formatted");
		}
	}

	public double attitudes(String to, String from) {
		if (convTable.containsKey(to) && convTable.containsKey(from))
			return (long) (convTable.get(from) * 1. / convTable.get(to) * 10000) * 1. / 10000;
		return 0;
	}

	public double attitudes(String to, String from, String date) {
		if (history.containsKey(date)) {
			Map<String, Long> tmp = convTable;
			convTable = history.get(date);
			double res = attitudes(to, from);
			convTable = tmp;
			return res;
		}
		return 0;
	}

	public double attitudes(String to, String from, Date date) {
		String dateStr = dateFormater.format(date);
		return attitudes(to, from, dateStr);
	}

	public double attitudesExtrapolation(String to, String from, Date date) throws ParseException {
		if (to.equals(from))
			return 1;
		if (history.containsKey(date))
			return attitudes(to, from, date);


		List<Double> x = new ArrayList<>();
		List<Double> y = new ArrayList<>();

		Date last = null;
		double lastX = 0;

		Date startD = null;
		for (Map.Entry<String, Map<String, Long>> day : history.entrySet()) {

			Date d = dateFormater.parse(day.getKey());

			if (last != null) {

				long diff = Math.abs(last.getTime() - d.getTime());
				long diffDays = diff / (24 * 60 * 60 * 1000);
				lastX += diffDays;
			} else {
				startD = d;
			}
			last = d;
			x.add(lastX);
			y.add((history.get(day.getKey()).get(from) * 1. / history.get(day.getKey()).get(to) * 10000));
		}
		Polynomial a = Polynomial.Newton(x, y);

		long diff = Math.abs(startD.getTime() - date.getTime());
		long diffDays = diff / (24 * 60 * 60 * 1000);
		return a.calc(diffDays);
	}
}
