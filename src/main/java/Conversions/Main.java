package Conversions;

import java.io.File;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Main {
	private static final String pattern = "^(?<acronym>[A-Z]+)\\W(?<full>.+)\\W+\\d+\\.\\d+\\W(?<to>\\d+\\.\\d{4})|(?<date>\\d{4}-\\d{2}-\\d{2})";

	public static void main(String[] args) throws Exception {
		File file = new File(Main.class.getResource("1/2018-02-12").getPath());

		Scanner in = new Scanner(file);
		Pattern pat = Pattern.compile(pattern);
		while (in.hasNextLine()) {
			Matcher mat = pat.matcher(in.nextLine());
			mat.find();
			if (mat.group("date") != null) {
				String date = mat.group("date");
			} else if (mat.group("acronym") != null && mat.group("full") != null && mat.group("to") != null) {
				String acronym = mat.group("acronym");
				String full = mat.group("full");
				String to = mat.group("to");
				double val = Double.parseDouble(to);
				System.out.println(acronym + " " + full + " " + to);
			} else throw new Exception("input File not formatted");
		}
	}
}
